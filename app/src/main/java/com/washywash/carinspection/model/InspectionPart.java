package com.washywash.carinspection.model;


public enum InspectionPart {

    CAR_RIGHT,
    CAR_LEFT,
    CAR_TOP,
    CART_FRONT_REAR,
    CAR_INTERIOR
}
