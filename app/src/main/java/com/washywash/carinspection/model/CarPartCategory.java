package com.washywash.carinspection.model;

public enum CarPartCategory {

    SIDE,
    TOP,
    INTERIOR
}
