package com.washywash.carinspection.model;

import android.text.TextUtils;
import android.util.Pair;

import com.washywash.carinspection.R;

import java.util.Calendar;

public class FormInformation {

    private String orderID;
    private Calendar inspectionDate;
    private String userName;
    private String userPhoneNumber;
    private String carPlate;
    private String email;
    private Pair<Double, Double> userLocation;
    private String street;
    private String buildingNumber;
    private String vehicleMake;
    private String vehicleModel;
    private int year = -1;

    public int validateData() {
        if (TextUtils.isEmpty(orderID)) {
                return R.string.order_id_missing;
        } else if (TextUtils.isEmpty(userName)) {
            return R.string.user_name_missing;
        } else if (TextUtils.isEmpty(userPhoneNumber)) {
            return R.string.phone_missing;
        } else if (TextUtils.isEmpty(carPlate)) {
            return R.string.car_plate_missing;
        } else if (TextUtils.isEmpty(email)) {
            return R.string.email_missing;
        } else if (TextUtils.isEmpty(street)) {
            return R.string.street_missing;
        } else if (TextUtils.isEmpty(buildingNumber)) {
            return R.string.building_number_missing;
        } else if (TextUtils.isEmpty(vehicleMake)) {
            return R.string.vehicle_make_missing;
        } else if (TextUtils.isEmpty(vehicleModel)) {
            return R.string.vehicle_model_missing;
        }  else if (year == -1) {
            return R.string.year_missing;
        } else if (inspectionDate == null) {
            return R.string.inspection_date_missing;
        } else{
            return -1;
        }

//        else if (userLocation == null) {
//            return R.string.user_lcoation_missing;
//        }
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public Calendar getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(Calendar inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getCarPlate() {
        return carPlate;
    }

    public void setCarPlate(String carPlate) {
        this.carPlate = carPlate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Pair<Double, Double> getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(Pair<Double, Double> userLocation) {
        this.userLocation = userLocation;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
