package com.washywash.carinspection.model;

import com.google.gson.annotations.SerializedName;

public class InspectionItem {

    @SerializedName("item_id")
    private int notificationId;
    @SerializedName("item_title")
    private String title;
    private boolean isSelected;
    private String letters;
    private String color;

    public InspectionItem(String title, String color) {
        this.title = title;
        this.color = color;
    }

    public InspectionItem() {
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
