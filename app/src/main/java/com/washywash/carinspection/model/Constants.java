package com.washywash.carinspection.model;

public class Constants {


    public static final String SUPPORT_EMAIL = "support@washywash.com";

//    public static String[] SideTopInspectionItems = {"Swirls - جروح دهان",
//            "Water spots - بقع ماء",
//            "Oxidation - اكسدة دهان",
//            "Rock Chips - نقرة حجر",
//            "Holograms - هولوغرام",
//            "Deep scratch - شحطه عميقه ",
//            "Paint transfer - بقع دهان",
//            "Clearcoat failure - ضرر في طبقه الجلاتين",
//            "Cloudy headlights - اضويه مغشيه",
//            "Dents - طعجات", "Wheel Damage - ضرر في العجلات",
//            "Loose trims - شعار غير ثابت او ناقص",
//            "Road Tar - زفتة شارع"};

  //  String[] colors = {"FDFB68", "73F7F9", "77F75F", "9F7D5C", "DC623E", "D83D77", "52A9D8", "98C15E", "4AA39B", "838BBD", "EDB05D", "BE9B76", "999999"};


    public static InspectionItem[] SideTopInspectionItems = {new InspectionItem("Swirls - جروح دهان", "#FDFB68"),
            new InspectionItem("Water spots - بقع ماء", "#73F7F9"),
            new InspectionItem("Oxidation - اكسدة دهان", "#77F75F"),
            new InspectionItem("Rock Chips - نقرة حجر", "#9F7D5C"),
            new InspectionItem("Holograms - هولوغرام", "#DC623E"),
            new InspectionItem("Deep scratch - شحطه عميقه ", "#D83D77"),
            new InspectionItem("Paint transfer - بقع دهان", "#52A9D8"),
            new InspectionItem("Clearcoat failure - ضرر في طبقه الجلاتين", "#98C15E"),
            new InspectionItem("Cloudy headlights - اضويه مغشيه", "#4AA39B"),
            new InspectionItem("Dents - طعجات", "#838BBD"),
            new InspectionItem("Wheel Damage - ضرر في العجلات", "#EDB05D"),
            new InspectionItem("Loose trims - شعار غير ثابت او ناقص", "#BE9B76"),
            new InspectionItem("Road Tar - زفتة شارع", "#999999")};


    public static InspectionItem[] InteriorInspectionItems = {
            new InspectionItem("Light soil - قليل الإتساخ ", "#73F7F9"),
            new InspectionItem("Medium soil - متوسط الإتساخ", "#77F75F"),
            new InspectionItem("Heavy soil - شديد الإتساخ", "#FCE851"),
            new InspectionItem("Pet hair - شعر حيوانات", "#9C5D6B"),
            new InspectionItem("Tear - مزع", "#DE4CC5"),
            new InspectionItem("Breakage - كسر", "#6951E0"),
            new InspectionItem("Leather damage - ضرر في الجلد", "#7EDC56"),
            new InspectionItem("Vinyl damage - ضرر فينيل", "#C1A358"),
            new InspectionItem("Fabric damage - ضرر قماش", "#6BA633"),
            new InspectionItem("Tannin stains - بقع شاي_قهوة", "#96C3A6"),
            new InspectionItem("Protein stains - بقع طعام", "#E95B53"),
            new InspectionItem("Odors - رائحه", "#BA7882"),
            new InspectionItem("Cigarette smoke - رائحه دخان", "#999999")
    };


    public static String API_KEY = "AIzaSyA1E6MaYQAgOYwnbZpCsjQIww6O0XzXvts";
}
