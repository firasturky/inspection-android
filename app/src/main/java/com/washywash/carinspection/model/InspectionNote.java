package com.washywash.carinspection.model;

import android.util.Pair;


public class InspectionNote {

    private InspectionItem inspectionItem;
    private String note;
    private Pair<Integer,Integer> coordinates;
    private InspectionPart inspectionPart;
    private int id;

    public InspectionNote(InspectionItem inspectionItem, String note, Pair<Integer, Integer> coordinates, InspectionPart inspectionPart, int id) {
        this.inspectionItem = inspectionItem;
        this.note = note;
        this.coordinates = coordinates;
        this.inspectionPart = inspectionPart;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public InspectionItem getInspectionItem() {
        return inspectionItem;
    }

    public void setInspectionItem(InspectionItem inspectionItem) {
        this.inspectionItem = inspectionItem;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Pair<Integer, Integer> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Pair<Integer, Integer> coordinates) {
        this.coordinates = coordinates;
    }

    public InspectionPart getInspectionPart() {
        return inspectionPart;
    }

    public void setInspectionPart(InspectionPart inspectionPart) {
        this.inspectionPart = inspectionPart;
    }
}
