package com.washywash.carinspection.fargments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.washywash.carinspection.databinding.Fragment1LayoutBinding;
import com.washywash.carinspection.manager.FormDataManager;
import com.washywash.carinspection.model.InspectionPart;

import static com.washywash.carinspection.manager.ImageManager.viewToBitmap;

public class Fragment1 extends BaseFragment {

    private Fragment1LayoutBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = Fragment1LayoutBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.CarLeftImageImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                showInspectionNoteDialog(motionEvent.getX(), motionEvent.getY(),
                        InspectionPart.CAR_RIGHT, binding.CarLeftRelativeLayout, view);

                return false;
            }
        });

        binding.CarRightImageImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                showInspectionNoteDialog(motionEvent.getX(), motionEvent.getY(),
                        InspectionPart.CAR_RIGHT, binding.CarRightRelativeLayout, view);
                return false;
            }
        });


        return view;
    }

    @Override
    public void saveImagesForFormManager() {
        FormDataManager.getInstance().setCarLeftBitmap(viewToBitmap(binding.CarLeftRelativeLayout));
        FormDataManager.getInstance().setCarRightBitmap(viewToBitmap(binding.CarRightRelativeLayout));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public static Fragment1 newInstance(int page, String title) {
        Fragment1 fragmentFirst = new Fragment1();
        Bundle args = new Bundle();
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }
}
