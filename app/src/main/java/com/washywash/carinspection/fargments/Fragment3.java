package com.washywash.carinspection.fargments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.washywash.carinspection.databinding.Fragment3LayoutBinding;
import com.washywash.carinspection.manager.FormDataManager;
import com.washywash.carinspection.model.InspectionPart;

import static com.washywash.carinspection.manager.ImageManager.viewToBitmap;

public class Fragment3 extends BaseFragment {

    private Fragment3LayoutBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = Fragment3LayoutBinding.inflate(inflater, container, false);
        View view = binding.getRoot();


        binding.CarInteriorImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                showInspectionNoteDialog(motionEvent.getX(), motionEvent.getY(),
                        InspectionPart.CAR_INTERIOR, binding.CarInteriorRelativeLayout, view);
                return false;
            }
        });

        return view;
    }

    public static Fragment3 newInstance(int page, String title) {
        Fragment3 fragmentFirst = new Fragment3();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void saveImagesForFormManager() {
        FormDataManager.getInstance().setCarInteriorBitmap(viewToBitmap(binding.CarInteriorRelativeLayout));
    }
}
