package com.washywash.carinspection.fargments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.washywash.carinspection.databinding.Fragment2LayoutBinding;
import com.washywash.carinspection.manager.FormDataManager;
import com.washywash.carinspection.model.InspectionPart;

import static com.washywash.carinspection.manager.ImageManager.viewToBitmap;

public class Fragment2 extends BaseFragment {

    private Fragment2LayoutBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = Fragment2LayoutBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.CarTopImageImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                showInspectionNoteDialog(motionEvent.getX(), motionEvent.getY(),
                        InspectionPart.CAR_TOP, binding.CarTopRelativeLayout, view);

                return false;
            }
        });

        binding.CarRearFrontImageImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                showInspectionNoteDialog(motionEvent.getX(), motionEvent.getY(),
                        InspectionPart.CART_FRONT_REAR, binding.CarRearFrontRelativeLayout, view);
                return false;
            }
        });


        return view;
    }

    public static Fragment2 newInstance(int page, String title) {
        Fragment2 fragmentFirst = new Fragment2();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void saveImagesForFormManager() {
        FormDataManager.getInstance().setCarTopBitmap(viewToBitmap(binding.CarTopRelativeLayout));
        FormDataManager.getInstance().setCarrearFrontBitmap(viewToBitmap(binding.CarRearFrontRelativeLayout));
    }
}
