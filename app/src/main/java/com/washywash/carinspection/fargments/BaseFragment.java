package com.washywash.carinspection.fargments;

import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.washywash.carinspection.dialogs.AddInspectionItemDialog;
import com.washywash.carinspection.manager.InspectionItemsManager;
import com.washywash.carinspection.model.InspectionPart;

public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void showInspectionNoteDialog(float x, float y, InspectionPart inspectionPart, RelativeLayout view, View clickedView) {
        if (InspectionItemsManager.getInstance().getCurrentSelectedInspectionItem() == null){
            return;
        }

        AddInspectionItemDialog addInspectionItemDialog = new AddInspectionItemDialog();
        addInspectionItemDialog.setMenuVisibility(false);
        addInspectionItemDialog.setCancelable(true);
        addInspectionItemDialog.setCoordinates(new Pair<>(x, y));
        addInspectionItemDialog.setInspectionPart(inspectionPart);
        addInspectionItemDialog.setView(view);
        addInspectionItemDialog.setClickedView(clickedView);
        addInspectionItemDialog.show(getFragmentManager(),"");
    }

    public abstract void saveImagesForFormManager();
}
