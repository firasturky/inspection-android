//package com.washywash.carinspection;
//
//import android.content.ClipData;
//import android.graphics.Color;
//import android.graphics.Rect;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.DragEvent;
//import android.view.Gravity;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import androidx.viewpager.widget.ViewPager;
//
//import com.washywash.carinspection.adapter.InspectionPagerAdapter;
//import com.washywash.carinspection.model.InspectionItem;
//import com.washywash.carinspection.views.InspectionItemsRecyclerView;
//
//import java.util.ArrayList;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//
//public class test {
//
//    package com.washywash.carinspection;
//
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.viewpager.widget.ViewPager;
//
//import android.content.ClipData;
//import android.graphics.Color;
//import android.graphics.Rect;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.DragEvent;
//import android.view.Gravity;
//import android.view.View;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.washywash.carinspection.adapter.InspectionPagerAdapter;
//import com.washywash.carinspection.model.InspectionItem;
//import com.washywash.carinspection.views.InspectionItemsRecyclerView;
//
//import java.util.ArrayList;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//
//    public class NotesActivity extends BaseActivity implements InspectionItemsRecyclerView.InspectionItemListener {
//
//        //    @BindView(R.id.CarLeftImage_ImageView)
////    ImageView carLeftImageImageView;
////    @BindView(R.id.CarRightImage_ImageView)
////    ImageView carRightImageImageView;
//        @BindView(R.id.MainLayout_RelativeLayout)
//        RelativeLayout mainLayout;
//        @BindView(R.id.InspectionItemsRecyclerView)
//        InspectionItemsRecyclerView inspectionItemsRecyclerView;
//        @BindView(R.id.Inspection_ViewPager)
//        ViewPager inspectionViewPager;
//
//
//        private View.OnDragListener mOnDragListener = new View.OnDragListener() {
//            @Override
//            public boolean onDrag(View v, DragEvent event) {
//
//                ImageView imageView = (ImageView) v;
//
//                switch (event.getAction()) {
//                    case DragEvent.ACTION_DRAG_STARTED:
//                        imageView.setColorFilter(Color.argb(100, 255, 0, 0)); // Red
//
//
//                        break;
//
//                    case DragEvent.ACTION_DRAG_ENTERED:
//                        imageView.setColorFilter(Color.argb(100, 0, 255, 0)); // Green
//                        break;
//
//                    case DragEvent.ACTION_DRAG_EXITED:
//                        imageView.setColorFilter(Color.argb(100, 0, 0, 255)); // Blue
//                        // mPositionTextView.setText("EXIT");
//                        break;
//
//                    case DragEvent.ACTION_DROP:
//
//                        ClipData.Item item = event.getClipData().getItemAt(0);
//                        CharSequence dragData = item.getText();
//                        Rect myViewRect = new Rect();
//                        imageView.getGlobalVisibleRect(myViewRect);
//                        float xx = myViewRect.left;
//                        float yx = myViewRect.top;
//
//                        int q = (int) (event.getX());
//                        int w = (int) event.getY();
//                        writeTextOnDrawable(q, w);
////                    writeTextOnDrawable(q, (int)xx);
//
//                        Log.i("TTT", "X: " + event.getX() + " Y: " + event.getY());
//                        //  writeTextOnDrawable((int)xx, (int)yx);
//
//                        break;
//
//                    case DragEvent.ACTION_DRAG_ENDED:
//                        imageView.setColorFilter(null);
//                        break;
//
//                    case DragEvent.ACTION_DRAG_LOCATION:
//                        float x = event.getX();
//                        float y = event.getY();
//
//                        String coordinate = String.format("[%.1f, %.1f]", x, y);
//                        //    mPositionTextView.setText(coordinate);
//                        break;
//                }
//
//
//                return true; // Change to Green
//            }
//        };
//
//        @Override
//        protected void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            setContentView(R.layout.activity_notes);
//            ButterKnife.bind(this);
//            inflateViews();
//            init();
//        }
//
//        private void init() {
//            inspectionItemsRecyclerView.setInspectionItemListener(this);
//        }
//
//        private void inflateViews() {
//
//            String[] names = {"Swirls - جروح دهان", "Water spots - بقع ماء", "Oxidation - اكسدة دهان", "Rock Chips - نقرة حجر",
//                    "Holograms - هولوغرام", "Deep scratch - شحطه عميقه ", "Paint transfer - بقع دهان", "Clearcoat failure - ضرر في طبقه الجلاتين",
//                    "Cloudy headlights - اضويه مغشيه", "Dents - طعجات", "Wheel Damage - ضرر في العجلات", "Loose trims - شعار غير ثابت او ناقص",
//                    "Road Tar - زفتة شارع"};
//
//            ArrayList<InspectionItem> inspectionItems = new ArrayList<>();
//
//            for (String name : names) {
//                InspectionItem inspectionItem = new InspectionItem();
//                inspectionItem.setTitle(name);
//                inspectionItems.add(inspectionItem);
//            }
//
//            inspectionItemsRecyclerView.setData(inspectionItems);
//
//            InspectionPagerAdapter adapterViewPager = new InspectionPagerAdapter(getSupportFragmentManager());
//            inspectionViewPager.setAdapter(adapterViewPager);
//
////        LinearLayout inspectionElementsLayout = findViewById(R.id.InspectionElements_LinearLayout);
////        carImageImageView.setOnDragListener(mOnDragListener);
////
////        for (int i = 0; i < names.length; i++) {
////            View child = getLayoutInflater().inflate(R.layout.inspection_item_layout, null, false);
////
////            TextView textView = child.findViewById(R.id.InspectionTitle_TextView);
////            CircularTextView circularTextView = child.findViewById(R.id.circularTextView);
////
////            textView.setText(names[i]);
////
////
////            circularTextView.setStrokeWidth(1);
////            circularTextView.setStrokeColor("#" + colors[i]);
////            circularTextView.setSolidColor("#" + colors[i]);
////            circularTextView.setText(names[i].substring(0, 2).toUpperCase());
////
////
////            circularTextView.setOnLongClickListener(new View.OnLongClickListener() {
////                @Override
////                public boolean onLongClick(View v) {
////                    Log.i("TTT", "Drag started from Image 1");
////                    ClipData dragData = ClipData.newPlainText("NoData", " mEditText.getText()");
////                    MyDragShadowBuilder myDragShadowBuilder = new MyDragShadowBuilder(v);
////                    v.startDrag(dragData, myDragShadowBuilder, null, 0);
////                    return true;
////                }
////            });
////
////            inspectionElementsLayout.addView(child);
////
//
//
////        }
//        }
//
//        private void writeTextOnDrawable(int xPos, int yPos) {
//
//            Rect myViewRect = new Rect();
//            //carImageImageView.getGlobalVisibleRect(myViewRect);
//            float left = myViewRect.left;
//            float top = myViewRect.top;
//
//            xPos = xPos + (int) left;
//            yPos = yPos;// + (int)top;
//
//
//            TextView theText = new TextView(this);
//            theText.setText("GG");
//            theText.setTextColor(Color.RED);
//            theText.setTypeface(Typeface.DEFAULT_BOLD);
//            theText.setTextSize(30);
//
//            Log.i("TTT", "DRAW X: " + xPos + " Y: " + yPos);
//
//            RelativeLayout.LayoutParams layoutParamsText = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//            layoutParamsText.leftMargin = xPos;
//            layoutParamsText.topMargin = yPos;
//            theText.setLayoutParams(layoutParamsText);
//
//
//            theText.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
//
//            mainLayout.addView(theText);
//
//        }
//
//        @Override
//        public void onRowClicked(InspectionItem inspectionItem, int position) {
//
//        }
//    }
//}
