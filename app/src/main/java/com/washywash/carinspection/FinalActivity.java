package com.washywash.carinspection;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;


import androidx.core.content.FileProvider;


import com.washywash.carinspection.databinding.ActivityFinalBinding;
import com.washywash.carinspection.manager.FormDataManager;
import com.washywash.carinspection.model.FormInformation;
import com.washywash.carinspection.utils.DateTimeUtils;

import java.io.File;
import java.util.Calendar;

import static com.washywash.carinspection.model.Constants.SUPPORT_EMAIL;


public class FinalActivity extends BaseActivity {

    private ActivityFinalBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFinalBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        init();
    }

    private void init() {
        String customerName = FormDataManager.getInstance().getFormInformation().getUserName();

        if (!TextUtils.isEmpty(customerName)) {
            binding.CustomerNameEditText.setText(customerName);
        }

        binding.BackTextView.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.CompleteTextView.setOnClickListener(view -> {
            SaveViewAndNavigateToPDFGenerator();
        });

        binding.ClearTextView.setOnClickListener(view -> {
            binding.signaturePad.clear();
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        binding.ClearTextView.setVisibility(View.VISIBLE);
        ShowPDFData();
    }

    private void ShowPDFData() {
        binding.PDFFileLayout.setVisibility(View.VISIBLE);
        binding.SendEmailLayout.setVisibility(View.VISIBLE);
        binding.PDFFileName.setText(FormDataManager.getInstance().getFileName());
        binding.PDFFileTimeCreation.setText(DateTimeUtils.getFormattedDate(Calendar.getInstance()));

        binding.PDFFileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPdfFile();
               // generateAndSendEmail();
            }
        });

        binding.SendEmailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // openPdfFile();
                 generateAndSendEmail();
            }
        });
    }

    private void  generateAndSendEmail() {

        FormInformation formInformation = FormDataManager.getInstance().getFormInformation();
        File file = new File(FormDataManager.getInstance().getPDFFileName());

        String subject = "Order#" + "  " + formInformation.getOrderID() + " Car Model " + formInformation.getVehicleModel() + " Plate#: " + formInformation.getCarPlate();
        String body = " Please find all the details attached in the email, for any question please contact us on this number # \n" +
                "or " + SUPPORT_EMAIL;
        String[] to = {formInformation.getEmail(), SUPPORT_EMAIL};

        sendEmail(subject, body, to, file);
    }

    private void openPdfFile() {
        File file = new File(FormDataManager.getInstance().getPDFFileName());
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        Uri data = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", file);

        target.setDataAndType(data, "application/pdf");


        target.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
        }
    }

    private void SaveViewAndNavigateToPDFGenerator() {
        binding.ClearTextView.setVisibility(View.INVISIBLE);
        binding.FinalPageLayout.setDrawingCacheEnabled(true);
        binding.FinalPageLayout.buildDrawingCache();
        Bitmap bitmap = binding.FinalPageLayout.getDrawingCache();
        FormDataManager.getInstance().setFinalImageBitmap(bitmap);
        goToNextActivity(PDFGenerateActivity.class);
    }
}