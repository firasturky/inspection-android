package com.washywash.carinspection.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

import com.washywash.carinspection.InfoFormActivity;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    public interface CalenderDateListener{
        public void onDateSelected(Calendar calendar);
    }

    private Calendar selectedCalender;
    private  CalenderDateListener calenderDateListener;

    public void setCalenderDateListener(CalenderDateListener calenderDateListener) {
        this.calenderDateListener = calenderDateListener;
    }

    public void setSelectedCalender(Calendar selectedCalender) {
        this.selectedCalender = selectedCalender;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int year = selectedCalender.get(Calendar.YEAR);
        int month = selectedCalender.get(Calendar.MONTH);
        int day = selectedCalender.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.getDatePicker().setMaxDate(selectedCalender.getTimeInMillis()+ (1000* 60*60*24));
        return dialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        selectedCalender.get(Calendar.YEAR);
        selectedCalender.get(Calendar.MONTH);
        selectedCalender.get(Calendar.DAY_OF_MONTH);
        if (calenderDateListener != null){
            calenderDateListener.onDateSelected(selectedCalender);
        }

    }


}