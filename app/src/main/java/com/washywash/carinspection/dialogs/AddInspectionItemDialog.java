package com.washywash.carinspection.dialogs;


import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.washywash.carinspection.CircularTextView;
import com.washywash.carinspection.R;
import com.washywash.carinspection.manager.InspectionItemsManager;
import com.washywash.carinspection.manager.PhotoEditorManager;
import com.washywash.carinspection.model.InspectionItem;
import com.washywash.carinspection.model.InspectionPart;
import com.washywash.carinspection.utils.UnitsConverter;

import java.io.FileNotFoundException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;
import static com.washywash.carinspection.manager.PhotoEditorManager.RESULT_LOAD_IMG;


public class AddInspectionItemDialog extends DialogFragment {


    private static final int REQUEST_IMAGE_CAPTURE = 90;
    private Dialog mDialog;
    private InspectionPart inspectionPart;
    private RelativeLayout mainRelativeLayout;
    private Pair<Float, Float> coordinates;
    private View clickedView;

    public void setCoordinates(Pair<Float, Float> coordinates) {
        this.coordinates = coordinates;
    }

    public void setInspectionPart(InspectionPart inspectionPart) {
        this.inspectionPart = inspectionPart;
    }


    public void setView(RelativeLayout mainRelativeLayout) {
        this.mainRelativeLayout = mainRelativeLayout;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (mDialog == null) {
            mDialog = new Dialog(getActivity());

            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            mDialog.setContentView(R.layout.layout_add_inspection_item);
            mDialog.getWindow().setGravity(Gravity.CENTER);


            View cancelIcon = mDialog.findViewById(R.id.CancelIcon_RelativeLayout);
            cancelIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });

            View addWithoutAttachment = mDialog.findViewById(R.id.AddWithoutAttachment_LinearLayout);
            View addFromCamera = mDialog.findViewById(R.id.AddFromCamera_LinearLayout);
            View addFromGallery = mDialog.findViewById(R.id.AddFromGallery_LinearLayout);

            View cancelTextView = mDialog.findViewById(R.id.Cancel_LinearLayout);

            cancelTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });

            addWithoutAttachment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                    addViewToLayout();
                }
            });

            addFromGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
                }
            });

            addFromCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dispatchTakePictureIntent();
                }
            });
        }
        return mDialog;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            // display error state to the user
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            InspectionItemsManager.getInstance().addCapturedImage(imageBitmap,
                    InspectionItemsManager.getInstance().getCurrentSelectedInspectionItem());
            addViewToLayout();
        }
        else if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                InspectionItemsManager.getInstance().addCapturedImage(selectedImage,
                        InspectionItemsManager.getInstance().getCurrentSelectedInspectionItem());
                addViewToLayout();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
        dismiss();
    }

    private void addViewToLayout() {
        InspectionItem inspectionItem = InspectionItemsManager.getInstance().getCurrentSelectedInspectionItem();
        View child = getLayoutInflater().inflate(R.layout.inspection_circle_layout, null, false);

        CircularTextView circularTextView = child.findViewById(
                R.id.circularTextView);

        circularTextView.setText(inspectionItem.getLetters());
        circularTextView.setStrokeWidth(1);
        circularTextView.setStrokeColor(inspectionItem.getColor());
        circularTextView.setSolidColor(inspectionItem.getColor());

        int x = clickedView.getLeft();

        int circularInspectionItemWH = (int) UnitsConverter.convertDpToPixels(getContext(),
                (int) getResources().getDimension(R.dimen.InspectionItemWH));
        circularInspectionItemWH = circularInspectionItemWH / 2;

        child.setX(coordinates.first - circularInspectionItemWH);
        child.setY(coordinates.second - circularInspectionItemWH);
        mainRelativeLayout.addView(child);
    }

    public void setClickedView(View clickedView) {
        this.clickedView = clickedView;
    }
}
