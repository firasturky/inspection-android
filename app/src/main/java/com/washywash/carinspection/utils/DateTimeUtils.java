package com.washywash.carinspection.utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Adam Zenaty on 12/10/17.
 */

public class DateTimeUtils {

    public static String DATE_YEAR_MONTH_DAY_WITH_DASHES = "yyyy-MM-dd";

    public static String getFormattedDate(Calendar calendar) {

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);
        return sdf.format(calendar.getTime());
    }

    public static String getFormattedDate1(Calendar calendar) {

        return getFormattedDateFromCalender(calendar, "EEE, d MMM yyyy, HH:mm");
    }


    public static String getFormattedDateWithoutTime(Calendar calendar) {

        return getFormattedDateFromCalender(calendar, DATE_YEAR_MONTH_DAY_WITH_DASHES);
    }


    public static String getFormattedDateFromCalender(Calendar calendar, String format) {

        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        return sdf.format(calendar.getTime());
    }

    public static Calendar getCalendarWithoutTime() {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    public static Calendar getCalenderFromHourMinute(Calendar currentCalendar, String hourString) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
        try {
            Date date = format.parse(hourString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            int Hr24 = calendar.get(Calendar.HOUR_OF_DAY);
            int Min = calendar.get(Calendar.MINUTE);

            currentCalendar.set(Calendar.HOUR_OF_DAY, Hr24);
            currentCalendar.set(Calendar.MINUTE, Min);
            return currentCalendar;
        } catch (ParseException e) {
            return null;
        }
    }

    public static Calendar getCalenderFromDate(String dateString, String hourString) {
        return getCalenderFromDate(dateString, hourString, null);
    }

    public static Calendar getCalenderFromDate(String dateString, String hourString, String dateFormat) {

        SimpleDateFormat format = null;

        if (TextUtils.isEmpty(dateFormat)) {
            format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);

        } else {
            format = new SimpleDateFormat(dateFormat, Locale.US);
        }

        try {
            if (TextUtils.isEmpty(dateString) || TextUtils.isEmpty(hourString)) {
                return null;
            }
            StringBuilder stringBuilder = new StringBuilder(dateString);
            stringBuilder.append(" ");
            stringBuilder.append(hourString);

            String fullDate = stringBuilder.toString();

            Date date = format.parse(fullDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            return calendar;
        } catch (ParseException e) {
            return null;
        }
    }

    public static boolean isTodayEqualGivenDay(Calendar selectedDate, int today) {

        if (selectedDate == null) {
            return false;
        }

        if (selectedDate.get(Calendar.DAY_OF_WEEK) == today) {
            return true;
        } else {
            return false;
        }
    }
}
