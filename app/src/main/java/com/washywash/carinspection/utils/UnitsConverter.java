package com.washywash.carinspection.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;



public class UnitsConverter {

    public static float convertDpToPixels(Context context, int unitInDp){

        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unitInDp, r.getDisplayMetrics());
    }
}
