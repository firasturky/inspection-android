package com.washywash.carinspection.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Adam Zenaty on 1/3/18.
 */

public class BaseRecyclerView extends RecyclerView {

    private boolean isLoading;
    private LoadMoreListener loadMoreListener;

    public void setLoadMoreListener(LoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public BaseRecyclerView(Context context) {
        super(context);
    }

    public BaseRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void addLoadMore(Adapter adapter, final LinearLayoutManager linearLayoutManager) {

        if (adapter != null) {

            addOnScrollListener(new OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    int visibleItemCount = linearLayoutManager.getChildCount();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();


                    if (!isLoading && ((visibleItemCount + pastVisibleItems) >= totalItemCount)) {

                        isLoading = true;
                        if (loadMoreListener != null) {
                            loadMoreListener.onLoadMore();
                        }
                    }

                    // Todo remove it after being sure that the new method is working fine 16-jan
//                    if (!isLoading && linearLayoutManager.findLastCompletelyVisibleItemPosition() == totalItem - 1) {
//                        //if (!isLoading && lastVisibleItem > 0 && lastVisibleItem == totalItem - 1) {
//                        isLoading = true;
//                        if (loadMoreListener != null) {
//                            loadMoreListener.onLoadMore();
//                        }
//                    }
                }
            });
        }
    }
}
