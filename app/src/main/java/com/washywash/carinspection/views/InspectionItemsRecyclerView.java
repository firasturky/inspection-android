package com.washywash.carinspection.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.washywash.carinspection.CircularTextView;
import com.washywash.carinspection.R;
import com.washywash.carinspection.model.InspectionItem;


public class InspectionItemsRecyclerView extends BaseRecyclerView {

    private Context mContext;
    private InspectionItem[] inspectionItems;
    private LayoutInflater mLayoutInflater;
    private HomeMenuAdapter adapter;
    private int selectedIndex = 0;
    private InspectionItemListener inspectionItemListener;


    public void setInspectionItemListener(InspectionItemListener inspectionItemListener) {
        this.inspectionItemListener = inspectionItemListener;
    }

    public InspectionItemsRecyclerView(Context context) {
        super(context);
        mContext = context;
    }

    public InspectionItemsRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public InspectionItemsRecyclerView(Context context, AttributeSet attrs, int defstyle) {
        super(context, attrs, defstyle);
        mContext = context;
    }

    public void setData(InspectionItem[] object) {
        inspectionItems = object;
        init();
    }

    public InspectionItem[] getData() {
        return inspectionItems;
    }


    public void init() {

        mLayoutInflater = LayoutInflater.from(mContext);
        if (adapter == null) {
            adapter = new HomeMenuAdapter();
            setAdapter(adapter);
            setLayoutManager();
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private void setLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext,
                RecyclerView.VERTICAL, false);
        setLayoutManager(linearLayoutManager);
        addLoadMore(adapter, linearLayoutManager);
    }

    private class HomeMenuAdapter extends Adapter {

        @Override
        public InspectionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = mLayoutInflater.inflate(R.layout.inspection_item_layout, parent, false);
            return new InspectionItemViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

            final InspectionItem inspectionItem = inspectionItems[position];
            inspectionItem.setLetters(inspectionItem.getTitle().substring(0, 2).toUpperCase());
           // inspectionItem.setColor("#" + inspectionItem.getColor());
            final InspectionItemViewHolder inspectionItemViewHolder = (InspectionItemViewHolder) holder;

            inspectionItemViewHolder.inspectionTitleTextView.setText(inspectionItem.getTitle());
            inspectionItemViewHolder.circularTextView.setText(inspectionItem.getLetters());
            inspectionItemViewHolder.circularTextView.setStrokeWidth(1);
            inspectionItemViewHolder.circularTextView.setStrokeColor(inspectionItem.getColor());
            inspectionItemViewHolder.circularTextView.setSolidColor(inspectionItem.getColor());


            holder.itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (inspectionItemListener != null) {
                        resetClicks();
                        inspectionItem.setSelected(true);
                        notifyDataSetChanged();
                        inspectionItemListener.onRowClicked(inspectionItem, position);
                    }
                }
            });

            if (inspectionItem.isSelected()) {
                holder.itemView.setBackgroundResource(R.drawable.background_item_selected);
            } else {
                holder.itemView.setBackground(null);
            }
        }

        @Override
        public int getItemCount() {
            return getData().length;
        }

        private void resetClicks() {

            for (InspectionItem inspectionItem : inspectionItems){
                inspectionItem.setSelected(false);
            }
        }
    }

    public void notifyDataItemChange() {

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void resetClicks() {
        if (adapter != null) {
            adapter.resetClicks();
        }
        notifyDataItemChange();
    }

    private class InspectionItemViewHolder extends ViewHolder {

        TextView inspectionTitleTextView;
        CircularTextView circularTextView;


        InspectionItemViewHolder(View itemView) {
            super(itemView);
            inspectionTitleTextView = itemView.findViewById(
                    R.id.InspectionTitle_TextView);
            circularTextView = itemView.findViewById(
                    R.id.circularTextView);

        }
    }

    public interface InspectionItemListener {

        void onRowClicked(InspectionItem inspectionItem, int position);
    }
}
