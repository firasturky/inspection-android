package com.washywash.carinspection.views;

/**
 * Created by Adam Zenaty on 1/3/18.
 */

public interface LoadMoreListener {

    void onLoadMore();
}
