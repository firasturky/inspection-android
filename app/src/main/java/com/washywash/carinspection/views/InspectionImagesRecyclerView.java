package com.washywash.carinspection.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.washywash.carinspection.CircularTextView;
import com.washywash.carinspection.R;
import com.washywash.carinspection.model.InspectionItem;

import java.util.ArrayList;


public class InspectionImagesRecyclerView extends BaseRecyclerView {

    private Context mContext;
    private ArrayList<Pair<Bitmap, InspectionItem>> inspectionItems;
    private LayoutInflater mLayoutInflater;
    private HomeMenuAdapter adapter;
    private int selectedIndex = 0;
    String[] colors = {"FDFB68", "73F7F9", "77F75F", "9F7D5C", "DC623E", "D83D77", "D73C77", "52A9D8", "98C15E", "4AA39B", "838BBD", "EDB05D", "BE9B76", "999999"};

    public InspectionImagesRecyclerView(Context context) {
        super(context);
        mContext = context;
    }

    public InspectionImagesRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public InspectionImagesRecyclerView(Context context, AttributeSet attrs, int defstyle) {
        super(context, attrs, defstyle);
        mContext = context;
    }

    public void setData(ArrayList<Pair<Bitmap, InspectionItem>> object) {
        inspectionItems = object;
        init();
    }

    public ArrayList<Pair<Bitmap, InspectionItem>> getData() {
        return inspectionItems;
    }


    public void init() {

        mLayoutInflater = LayoutInflater.from(mContext);
        if (adapter == null) {
            adapter = new HomeMenuAdapter();
            setAdapter(adapter);
            setLayoutManager();
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private void setLayoutManager() {
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 4);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        setLayoutManager(layoutManager);
    }

    private class HomeMenuAdapter extends Adapter {

        @Override
        public InspectionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = mLayoutInflater.inflate(R.layout.inspection_images_layout, parent, false);
            return new InspectionItemViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

            Pair<Bitmap, InspectionItem> inspectionPair = inspectionItems.get(position);
            InspectionItem inspectionItem = inspectionPair.second;
            inspectionItem.setLetters(inspectionItem.getTitle().substring(0, 2).toUpperCase());
            final InspectionItemViewHolder inspectionItemViewHolder = (InspectionItemViewHolder) holder;

            inspectionItemViewHolder.inspectionImageView.setImageBitmap(inspectionPair.first);
            inspectionItemViewHolder.circularTextView.setText(inspectionItem.getLetters());
            inspectionItemViewHolder.circularTextView.setStrokeWidth(1);
            inspectionItemViewHolder.circularTextView.setStrokeColor(inspectionItem.getColor());
            inspectionItemViewHolder.circularTextView.setSolidColor(inspectionItem.getColor());
        }

        @Override
        public int getItemCount() {
            return getData().size();
        }

    }

    private class InspectionItemViewHolder extends ViewHolder {

        ImageView inspectionImageView;
        CircularTextView circularTextView;


        InspectionItemViewHolder(View itemView) {
            super(itemView);
            inspectionImageView = itemView.findViewById(
                    R.id.InspectionImageView);
            circularTextView = itemView.findViewById(
                    R.id.circularTextView);

        }
    }

    public interface InspectionItemListener {

        void onRowClicked(InspectionItem inspectionItem, int position);
    }
}
