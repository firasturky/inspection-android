package com.washywash.carinspection;

import android.graphics.Bitmap;
import android.os.Bundle;


import com.washywash.carinspection.databinding.ActivityGeneralNotesBinding;
import com.washywash.carinspection.manager.FormDataManager;

public class GeneralNotesActivity extends BaseActivity {

    private ActivityGeneralNotesBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityGeneralNotesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.BackTextView.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.NextTextView.setOnClickListener(view -> {
            binding.GeneralNotesLinearLayout.setDrawingCacheEnabled(true);
            binding.GeneralNotesLinearLayout.buildDrawingCache();
            Bitmap bitmap = binding.GeneralNotesLinearLayout.getDrawingCache();
            FormDataManager.getInstance().setGeneralNotesImageBitmap(bitmap);
            goToNextActivity(FinalActivity.class);
        });
    }
}