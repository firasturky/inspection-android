package com.washywash.carinspection;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.washywash.carinspection.adapter.InspectionPagerAdapter;
import com.washywash.carinspection.databinding.ActivityNotesBinding;
import com.washywash.carinspection.fargments.BaseFragment;
import com.washywash.carinspection.manager.FormDataManager;
import com.washywash.carinspection.manager.ImageManager;
import com.washywash.carinspection.manager.InspectionItemsManager;
import com.washywash.carinspection.model.CarPartCategory;
import com.washywash.carinspection.model.Constants;
import com.washywash.carinspection.model.InspectionItem;
import com.washywash.carinspection.views.InspectionItemsRecyclerView;

import java.util.ArrayList;

import static com.washywash.carinspection.model.Constants.InteriorInspectionItems;

public class CarPartsNotesActivity extends BaseActivity implements InspectionItemsRecyclerView.InspectionItemListener {


    private ActivityNotesBinding binding;
    private CarPartCategory previousSelectedCarPartCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNotesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        inflateViews();
        init();
    }

    private void init() {
        previousSelectedCarPartCategory = CarPartCategory.SIDE;

        binding.InspectionItemsRecyclerView.setInspectionItemListener(this);

        binding.BackTextView.setOnClickListener(view -> {
            getFragmentInstanceAndSaveImages();
            int currPos = binding.InspectionViewPager.getCurrentItem();
            binding.InspectionViewPager.setCurrentItem(currPos - 1);

        });

        binding.NextTextView.setOnClickListener(view -> {
            getFragmentInstanceAndSaveImages();
            if (getString(R.string.finish).equalsIgnoreCase(binding.NextTextView.getText().toString())) {
                binding.InspectionItemsRecyclerView.resetClicks();
                Bitmap bitmap = ImageManager.getScreenshotFromRecyclerView(binding.InspectionItemsRecyclerView);
                FormDataManager.getInstance().setRecyclerViewBitmap(bitmap);
                goToNextActivity(ImagesActivity.class);
            } else {
                int currPos = binding.InspectionViewPager.getCurrentItem();
                binding.InspectionViewPager.setCurrentItem(currPos + 1);
            }
        });

        binding.InspectionViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                handleBottomButtonNavigation(position);
                handleCurrentPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void handleCurrentPage(int position) {
        switch (position) {
            case 0:
                if (!isTheSameItemsContent(position)){
                    binding.InspectionItemsRecyclerView.setData(Constants.SideTopInspectionItems);
                }
                previousSelectedCarPartCategory = CarPartCategory.SIDE;
                break;
            case 1:
                previousSelectedCarPartCategory = CarPartCategory.TOP;
                if (!isTheSameItemsContent(position)){
                    binding.InspectionItemsRecyclerView.setData(Constants.SideTopInspectionItems);
                }
                break;
            case 2:
                previousSelectedCarPartCategory = CarPartCategory.INTERIOR;
                if (!isTheSameItemsContent(position)){
                    binding.InspectionItemsRecyclerView.setData(InteriorInspectionItems);
                }
                break;
        }
    }

    private boolean isTheSameItemsContent(int position) {

        if (position == 0 && previousSelectedCarPartCategory == CarPartCategory.TOP) {
            return true;
        } else if (position == 1 && previousSelectedCarPartCategory == CarPartCategory.SIDE) {
            return true;
        }else{
            return false;
        }
    }

    private void handleBottomButtonNavigation(int position) {

        if (position == binding.InspectionViewPager.getAdapter().getCount() - 1) {
            binding.NextTextView.setText(R.string.finish);
        } else if (position == 0) {
            binding.BackTextView.setClickable(false);
            binding.BackTextView.setEnabled(false);
        } else {
            binding.BackTextView.setEnabled(true);
            binding.BackTextView.setClickable(true);
            binding.NextTextView.setText(R.string.next);
        }

    }

    private void getFragmentInstanceAndSaveImages() {
        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:"
                + R.id.Inspection_ViewPager + ":" + binding.InspectionViewPager.getCurrentItem());

        if (page instanceof BaseFragment) {
            BaseFragment baseFragment = (BaseFragment) page;
            baseFragment.saveImagesForFormManager();

        }
    }

    private void getFragmentInstanceAndSaveImage() {
        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:"
                + R.id.Inspection_ViewPager + ":" + binding.InspectionViewPager.getCurrentItem());

        if (page instanceof BaseFragment) {
            BaseFragment baseFragment = (BaseFragment) page;
            baseFragment.saveImagesForFormManager();

        }
    }

    private void inflateViews() {

        binding.InspectionItemsRecyclerView.setData(Constants.SideTopInspectionItems);

        InspectionPagerAdapter adapterViewPager = new InspectionPagerAdapter(getSupportFragmentManager());
        binding.InspectionViewPager.setAdapter(adapterViewPager);
        binding.InspectionViewPager.setOffscreenPageLimit(3);
    }

    @Override
    public void onRowClicked(InspectionItem inspectionItem, int position) {
        InspectionItemsManager.getInstance().setCurrentSelectedInspectionItem(inspectionItem);
    }
}