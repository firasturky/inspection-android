package com.washywash.carinspection.manager;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.washywash.carinspection.R;

import java.io.File;
import java.io.FileOutputStream;

public class PDFManager {

    private PdfDocument document;
    private PdfDocument.PageInfo pageInfo;

    public void initPDF(Context context) {

        document = new PdfDocument();
        // crate a page description
        pageInfo = new PdfDocument.PageInfo
                .Builder((int) context.getResources().getDimension(R.dimen.pdf_page_width), (int) context.getResources().getDimension(R.dimen.pdf_page_height), 1).create();

    }


    public String createWholePDFFile(String fileName, Context context) {
        String filePathName= "";
        try {

            // write the document content
            String rootPath = context.getExternalFilesDir(null).getAbsolutePath() + "/mypdf/";
            File root = new File(rootPath);
            if (!root.exists()) {
                root.mkdirs();
            }

            File file = new File(rootPath + fileName);

            if (!file.exists()) {
                file.createNewFile();
            }
             filePathName = file.getAbsolutePath();
            document.writeTo(new FileOutputStream(file));
        } catch (Exception e) {
            Log.e("main", "error " + e.toString());
            // Toast.makeText(this, "Something wrong: " + e.toString(),  Toast.LENGTH_LONG).show();
        }

        // close the document
        document.close();
        return filePathName;
    }


    public void voidCreatePDFDocument(RelativeLayout formView) {

        // start a page
        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);

        formView.draw(page.getCanvas());
        document.finishPage(page);
    }


    public void voidCreatePDFDocument(ImageView formView) {

        // start a page
        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);

        formView.draw(page.getCanvas());
        document.finishPage(page);
    }
}
