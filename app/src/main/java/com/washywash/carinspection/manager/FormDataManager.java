package com.washywash.carinspection.manager;

import android.content.Context;
import android.graphics.Bitmap;

import com.washywash.carinspection.model.FormInformation;
import com.washywash.carinspection.utils.DateTimeUtils;

public class FormDataManager {

    private static FormDataManager instance;
    private FormInformation formInformation;

    private Bitmap carLeftBitmap;
    private Bitmap carRightBitmap;
    private Bitmap carTopBitmap;
    private Bitmap carrearFrontBitmap;
    private Bitmap carInteriorBitmap;
    private Bitmap recyclerViewBitmap;
    private Bitmap finalImageBitmap;
    private String PDFFileName;
    private Bitmap allImagesBitmap;
    private Bitmap generalNotesImageBitmap;

    public String getFormText(Context context){
        return DocumentDecorator.generateFormText(context, formInformation);
    }


    private static void initInstance() {
        if (instance == null) {
            instance = new FormDataManager();
        }
    }

    public static FormDataManager getInstance() {

        if (instance == null) {
            initInstance();
        }
        return instance;
    }

    public String getFileName(){
        FormInformation formInformation = FormDataManager.getInstance().getFormInformation();
        if (formInformation == null){
            return "";
        }else{
            return formInformation.getUserName() + "+" + DateTimeUtils.getFormattedDate(formInformation.getInspectionDate())+".pdf";
        }
    }

    public Bitmap getCarLeftBitmap() {
        return carLeftBitmap;
    }

    public void setCarLeftBitmap(Bitmap carLeftBitmap) {
        this.carLeftBitmap = carLeftBitmap;
    }

    public Bitmap getCarRightBitmap() {
        return carRightBitmap;
    }

    public void setCarRightBitmap(Bitmap carRightBitmap) {
        this.carRightBitmap = carRightBitmap;
    }

    public FormInformation getFormInformation() {
        return formInformation;
    }

    public void setFormInformation(FormInformation formInformation) {
        this.formInformation = formInformation;
    }


    public static void setInstance(FormDataManager instance) {
        FormDataManager.instance = instance;
    }

    public Bitmap getCarrearFrontBitmap() {
        return carrearFrontBitmap;
    }

    public void setCarrearFrontBitmap(Bitmap carrearFrontBitmap) {
        this.carrearFrontBitmap = carrearFrontBitmap;
    }

    public Bitmap getCarInteriorBitmap() {
        return carInteriorBitmap;
    }

    public void setCarInteriorBitmap(Bitmap carInteriorBitmap) {
        this.carInteriorBitmap = carInteriorBitmap;
    }

    public Bitmap getCarTopBitmap() {
        return carTopBitmap;
    }

    public void setCarTopBitmap(Bitmap carTopBitmap) {
        this.carTopBitmap = carTopBitmap;
    }

    public void setRecyclerViewBitmap(Bitmap recyclerViewBitmap) {
        this.recyclerViewBitmap = recyclerViewBitmap;
    }

    public Bitmap getRecyclerViewBitmap() {
        return recyclerViewBitmap;
    }

    public void setFinalImageBitmap(Bitmap finalImageBitmap) {
        this.finalImageBitmap = finalImageBitmap;
    }

    public Bitmap getFinalImageBitmap() {
        return finalImageBitmap;
    }

    public void setPDFFileName(String pdfFileName) {
        this.PDFFileName = pdfFileName;
    }

    public String getPDFFileName() {
        return PDFFileName;
    }

    public void setAllImagesBitmap(Bitmap allImagesBitmap) {

        this.allImagesBitmap = allImagesBitmap;
    }

    public Bitmap getAllImagesBitmap() {
        return allImagesBitmap;
    }

    public void setGeneralNotesImageBitmap(Bitmap generalNotesImageBitmap) {
        this.generalNotesImageBitmap = generalNotesImageBitmap;
    }

    public Bitmap getGeneralNotesImageBitmap() {
        return generalNotesImageBitmap;
    }
}
