package com.washywash.carinspection.manager;

import android.app.Activity;
import android.content.Intent;

public class PhotoEditorManager {

    public static final int RESULT_LOAD_IMG = 909;

    public static void getPhotoFromGallery(Activity activity){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        activity.startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
    }
}
