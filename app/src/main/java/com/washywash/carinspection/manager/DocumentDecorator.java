package com.washywash.carinspection.manager;

import android.app.Application;
import android.content.Context;

import com.washywash.carinspection.R;
import com.washywash.carinspection.model.FormInformation;
import com.washywash.carinspection.utils.DateTimeUtils;

public class DocumentDecorator {


    public static String generateFormText(Context context, FormInformation formInformation) {
        if (formInformation == null) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder("");


        stringBuilder.append(context.getString(R.string.order_id))
                .append(": ").append(formInformation.getOrderID()).append("\n");
        stringBuilder.append(context.getString(R.string.inspection_date)).append(": ").append(DateTimeUtils.
                getFormattedDate1(formInformation.getInspectionDate())).append("\n");


        stringBuilder.append(context.getString(R.string.name)).append(": ").append(formInformation.getUserName()).append("   ");
        stringBuilder.append(context.getString(R.string.phone)).append(": ").append((formInformation.getUserPhoneNumber())).append("\n");


        stringBuilder.append(context.getString(R.string.email)).append(": ").append(formInformation.getEmail()).append("   ");
        stringBuilder.append(context.getString(R.string.car_plate)).append(": ").append((formInformation.getCarPlate())).append("\n");

        stringBuilder.append(context.getString(R.string.street)).append(": ").append(formInformation.getStreet()).append("   ");
        stringBuilder.append(context.getString(R.string.building_number)).append(": ").append((formInformation.getBuildingNumber())).append("\n");

        stringBuilder.append(context.getString(R.string.vehicle_make)).append(": ").append(formInformation.getVehicleMake()).append("   ");
        stringBuilder.append(context.getString(R.string.model)).append(": ").append((formInformation.getVehicleModel())).append("\n");

        stringBuilder.append(context.getString(R.string.year)).append(": ").append(formInformation.getYear()).append("   ");

        return stringBuilder.toString();
    }
}
