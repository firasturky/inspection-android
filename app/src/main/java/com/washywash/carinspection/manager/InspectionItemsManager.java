package com.washywash.carinspection.manager;

import android.graphics.Bitmap;
import android.util.Pair;
import android.view.View;

import com.washywash.carinspection.model.InspectionItem;
import com.washywash.carinspection.model.InspectionNote;
import com.washywash.carinspection.model.InspectionPart;

import java.util.ArrayList;

import static com.washywash.carinspection.model.Constants.InteriorInspectionItems;

public class InspectionItemsManager {

    private static InspectionItemsManager instance;
    private ArrayList<InspectionNote> inspectionNotes;
    private InspectionItem currentSelectedInspectionItem;
    private ArrayList<Pair<Bitmap,InspectionItem>> capturedItemsWithImages;

    private InspectionItemsManager() {
        this.inspectionNotes = new ArrayList<>();
    }

    public static InspectionItemsManager getInstance() {

        if (instance == null) {
            initInstance();
        }
        return instance;
    }

    public void removeInspectionNotes(int id) {

        for (InspectionNote inspectionNote : inspectionNotes){
            if ( inspectionNote.getId() == id){
                inspectionNotes.remove(inspectionNote);
            }
        }
    }

    public InspectionNote addInspectionNote(InspectionItem inspectionItem, int x, int y, String notes, InspectionPart inspectionPart, int inspectionId) {
        InspectionNote inspectionNote = new InspectionNote(inspectionItem, notes, new Pair<>(x, y), inspectionPart, View.generateViewId());
        inspectionNotes.add(inspectionNote);
        return inspectionNote;
    }

    public InspectionItem getCurrentSelectedInspectionItem() {
        return currentSelectedInspectionItem;
    }

    public void setCurrentSelectedInspectionItem(InspectionItem currentSelectedInspectionItem) {
        this.currentSelectedInspectionItem = currentSelectedInspectionItem;
    }

    public static void initInstance() {
        if (instance == null) {
            instance = new InspectionItemsManager();
        }
    }

    public void addCapturedImage(Bitmap bitmap, InspectionItem inspectionItem){
        if (capturedItemsWithImages == null){
            capturedItemsWithImages =  new ArrayList<>();
        }

        capturedItemsWithImages.add(new Pair<>(bitmap, inspectionItem));
    }

    public ArrayList<Pair<Bitmap, InspectionItem>> getCapturedItemsWithImages(){
        return capturedItemsWithImages;
    }
}
