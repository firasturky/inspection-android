package com.washywash.carinspection;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.washywash.carinspection.databinding.ActivityInfoFormBinding;
import com.washywash.carinspection.dialogs.DatePickerFragment;
import com.washywash.carinspection.manager.FormDataManager;
import com.washywash.carinspection.model.FormInformation;
import com.washywash.carinspection.utils.DateTimeUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class InfoFormActivity extends BaseActivity implements DatePickerFragment.CalenderDateListener, OnMapReadyCallback {


    private ActivityInfoFormBinding binding;
    private Calendar selectedCalender = Calendar.getInstance();
    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private float DEFAULT_ZOOM = 5;
    private static final String TAG = "TAGE";
    private LatLng defaultLocation = new LatLng(-34, 151);
    private Location lastKnownLocation;
    private PlacesClient placesClient;
    int M_MAX_ENTRIES = 5;
    private LatLng selectedLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityInfoFormBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
    }

    private void init() {


        ArrayList<Character> stringArrayList = new ArrayList<>();
        String s = "abcabcv";
        char[] letters = s.toCharArray();
        String temporaryLetter = "";




        for(char c : letters){

            if (stringArrayList.contains(c)){

            }else{
                stringArrayList.add(c);
            }

           ;
        }



        binding.NextTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getInformationAndSaveIt();
            }
        });

        binding.InspectionDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.setSelectedCalender(selectedCalender);
                newFragment.setCalenderDateListener(InfoFormActivity.this);
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Places.initialize(getApplicationContext(), getString(R.string.api_key));
        // Create a new PlacesClient instance
        placesClient = Places.createClient(this);

        // Construct a FusedLocationProviderClient.
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }

    private void getStreetNameFromPoint(LatLng mPosition) {

        try {
            List<Address> addresses;
            Geocoder geocoder = new Geocoder(InfoFormActivity.this);
            addresses = geocoder.getFromLocation(mPosition.latitude, mPosition.longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                binding.StreetEditText.setText(address.getThoroughfare());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);

        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {

            Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {

                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.getResult();
                        if (lastKnownLocation != null) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(lastKnownLocation.getLatitude(),
                                            lastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                        }
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.");
                        Log.e(TAG, "Exception: %s", task.getException());
                        mMap.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(defaultLocation, DEFAULT_ZOOM));
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                    showCurrentPlace();
                }
            });

        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage(), e);
        }
    }

    private void showCurrentPlace() {
        if (mMap == null) {
            return;
        }

        // Use fields to define the data types to return.
        List<Place.Field> placeFields = Arrays.asList(Place.Field.NAME, Place.Field.ADDRESS,Place.Field.ID,
                Place.Field.LAT_LNG);

        // Use the builder to create a FindCurrentPlaceRequest.
        FindCurrentPlaceRequest request =
                FindCurrentPlaceRequest.newInstance(placeFields);


        // Get the likely places - that is, the businesses and other points of interest that
        // are the best match for the device's current location.
        @SuppressWarnings("MissingPermission") final Task<FindCurrentPlaceResponse> placeResult =
                placesClient.findCurrentPlace(request);
        placeResult.addOnCompleteListener(new OnCompleteListener<FindCurrentPlaceResponse>() {
            @Override
            public void onComplete(@NonNull Task<FindCurrentPlaceResponse> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    FindCurrentPlaceResponse likelyPlaces = task.getResult();

                    // Set the count, handling cases where less than 5 entries are returned.
                    int count;

                    if (likelyPlaces.getPlaceLikelihoods().size() < M_MAX_ENTRIES) {
                        count = likelyPlaces.getPlaceLikelihoods().size();
                    } else {
                        count = M_MAX_ENTRIES;
                    }

                    int i = 0;
                    String[] likelyPlaceNames = new String[count];
                    String[] likelyPlaceAddresses = new String[count];
                    List[] likelyPlaceAttributions = new List[count];
                    LatLng[] likelyPlaceLatLngs = new LatLng[count];

                    String p = null;
                    for (PlaceLikelihood placeLikelihood : likelyPlaces.getPlaceLikelihoods()) {



                        // Build a list of likely places to show the user.
                        likelyPlaceNames[i] = placeLikelihood.getPlace().getName();
                        likelyPlaceAddresses[i] = placeLikelihood.getPlace().getAddress();
                        likelyPlaceAttributions[i] = placeLikelihood.getPlace()
                                .getAttributions();
                        likelyPlaceLatLngs[i] = placeLikelihood.getPlace().getLatLng();

                        String address = placeLikelihood.getPlace().getAddress();
                        int indexOfAddress = address.indexOf(",");
                        if (indexOfAddress>0){
                            address = address.substring(0, indexOfAddress);
                        }

                        binding.StreetEditText.setText(address);

                        i++;
                        if (i > (count - 1)) {
                            break;
                        }
                    }


                } else {
                    Log.e(TAG, "Exception: %s", task.getException());
                }
            }
        });
    }

    private void getInformationAndSaveIt() {

        FormInformation formInformation = new FormInformation();
        formInformation.setOrderID(binding.OrderIdEditText.getText().toString().trim());
        formInformation.setInspectionDate(selectedCalender);
        formInformation.setUserName(binding.NextTextView.getText().toString().trim());
        formInformation.setUserPhoneNumber(binding.PhoneEditText.getText().toString().trim());
        formInformation.setEmail(binding.EmailEditText.getText().toString().trim());
        formInformation.setCarPlate(binding.CarPlateEditText.getText().toString().trim());
        formInformation.setUserLocation(null);
        formInformation.setStreet(binding.StreetEditText.getText().toString().trim());
        formInformation.setBuildingNumber(binding.BuildingNumberEditText.getText().toString().trim());
        formInformation.setVehicleMake(binding.VehicleMakeEditText.getText().toString().trim());
        formInformation.setVehicleModel(binding.ModelEditText.getText().toString().trim());
        formInformation.setYear(getIntegerFromString(binding.YearEditText.getText().toString().trim()));

       // formInformation = getDummyData();

        int formValidationResults = formInformation.validateData();
        if (formValidationResults == -1) {
            FormDataManager.getInstance().setFormInformation(formInformation);
            goToNextActivity(CarPartsNotesActivity.class);

        } else {
            binding.ErrorTextView.setText(formValidationResults);
        }


    }


    private FormInformation getDummyData() {
        FormInformation formInformation = new FormInformation();
        formInformation.setOrderID("0001");
        formInformation.setInspectionDate(selectedCalender);
        formInformation.setUserName("Adam Alzinati");
        formInformation.setUserPhoneNumber("0797445533");
        formInformation.setEmail("Adamzenaty@gmail.com");
        formInformation.setCarPlate("NCD-F36");
        formInformation.setUserLocation(new Pair<Double, Double>(1233.0, 123.0));
        formInformation.setStreet("No7 el romi");
        formInformation.setBuildingNumber("123");
        formInformation.setVehicleMake("Mercedes");
        formInformation.setVehicleModel("C Class");
        formInformation.setYear(2018);

        return formInformation;
    }

    @Override
    public void onDateSelected(Calendar calendar) {
        binding.InspectionDateEditText.setText(DateTimeUtils.getFormattedDateWithoutTime(calendar));
        selectedCalender = calendar;
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;

        updateLocationUI();
        getDeviceLocation();
        setCameraListener();
    }

    private void setCameraListener(){
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng mPosition = mMap.getCameraPosition().target;
                float mZoom = mMap.getCameraPosition().zoom;

                selectedLatLng = new LatLng(mPosition.latitude, mPosition.longitude);
                getStreetNameFromPoint(mPosition);
            }
        });
    }
}