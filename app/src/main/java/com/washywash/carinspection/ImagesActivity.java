package com.washywash.carinspection;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import com.washywash.carinspection.databinding.ActivityImagesBinding;
import com.washywash.carinspection.databinding.ActivityNotesBinding;
import com.washywash.carinspection.manager.FormDataManager;
import com.washywash.carinspection.manager.InspectionItemsManager;

public class ImagesActivity extends BaseActivity {

    private ActivityImagesBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityImagesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
       init();
    }

    private void init(){
        if (InspectionItemsManager.getInstance().getCapturedItemsWithImages() == null){
            binding.ErrorTextView.setVisibility(View.VISIBLE);
            navigateToNextActivity();
            finish();
        }else {
            binding.ErrorTextView.setVisibility(View.GONE);
            binding.InspectionItemsRecyclerView.setData(InspectionItemsManager.getInstance().getCapturedItemsWithImages());
        }

        binding.NextTextView.setOnClickListener(v -> {
            SaveViewAndNavigateToPDFGenerator();
        });
    }

    private void SaveViewAndNavigateToPDFGenerator(){

        if (InspectionItemsManager.getInstance().getCapturedItemsWithImages() != null){
            binding.ImageContentLayout.setDrawingCacheEnabled(true);
            binding.ImageContentLayout.buildDrawingCache();
            Bitmap bitmap = binding.ImageContentLayout.getDrawingCache();
            FormDataManager.getInstance().setAllImagesBitmap(bitmap);
        }

        navigateToNextActivity();
    }

    private void navigateToNextActivity(){
        goToNextActivity(GeneralNotesActivity.class);
    }
}