package com.washywash.carinspection.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.washywash.carinspection.fargments.Fragment1;
import com.washywash.carinspection.fargments.Fragment2;
import com.washywash.carinspection.fargments.Fragment3;

public class InspectionPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;

    public InspectionPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return Fragment1.newInstance(0, "Page # 1");
            case 1:
                return Fragment2.newInstance(1, "Page # 2");
            case 2:
                return Fragment3.newInstance(2, "Page # 3");
            default:
                return null;
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}
