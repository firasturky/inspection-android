package com.washywash.carinspection;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.washywash.carinspection.databinding.ActivityGeneratePdfBinding;
import com.washywash.carinspection.manager.FormDataManager;
import com.washywash.carinspection.manager.PDFManager;
import com.washywash.carinspection.model.FormInformation;
import com.washywash.carinspection.utils.DateTimeUtils;

import java.util.Timer;
import java.util.TimerTask;

public class PDFGenerateActivity extends AppCompatActivity {

    private ActivityGeneratePdfBinding binding;
    private Timer timer = new Timer();
    private int begin = 0;
    private int timeInterval = 2000;
    private PDFManager pdfManager;
    private boolean isReadyToGoNextPage = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityGeneratePdfBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        init();
    }

    private void generateAllPDFPages() {

        timer.schedule(new TimerTask() {
            int counter = 0;

            @Override
            public void run() {

                PDFGenerateActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!isReadyToGoNextPage) {
                            return;
                        }

                        showProgress(counter);

                        switch (counter) {
                            case 1:
                                createPage2();
                                break;
                            case 2:
                                createPage3();
                                break;
                            case 3:
                                createPage4();
                                break;
                            case 4:
                                createPage5();
                                break;
                            case 5:
                                createPage6();
                                break;
                            case 6:
                                createPage7();
                                break;
                        }

                        counter++;
                        if (counter > 7) {
                            FormDataManager.getInstance().setPDFFileName(pdfManager.createWholePDFFile
                                    (FormDataManager.getInstance().getFileName(), PDFGenerateActivity.this));
                            timer.cancel();
                            finish();
                        }
                    }
                });
            }
        }, begin, timeInterval);
    }

    private void createPage1() {
        fillPage1Info();
        binding.PDFContainer1.getRoot().post(new Runnable() {
            @Override
            public void run() {
                pdfManager.voidCreatePDFDocument(binding.PDFContainer1.getRoot());
            }
        });
    }

    private void createPage2() {

        binding.PDFContainer2.CarLeft.setImageBitmap(FormDataManager.getInstance().getCarLeftBitmap());
        binding.PDFContainer2.CarRight.setImageBitmap(FormDataManager.getInstance().getCarRightBitmap());
        binding.PDFContainer2.InspectionElementsImageView.setImageBitmap(FormDataManager.getInstance().getRecyclerViewBitmap());


        binding.PDFContainer2.getRoot().post(new Runnable() {
            @Override
            public void run() {
                pdfManager.voidCreatePDFDocument(binding.PDFContainer2.getRoot());
            }
        });
    }

    private void createPage3() {
        binding.PDFContainer2.CarLeft.setImageBitmap(FormDataManager.getInstance().getCarTopBitmap());
        binding.PDFContainer2.CarRight.setImageBitmap(FormDataManager.getInstance().getCarrearFrontBitmap());
        pdfManager.voidCreatePDFDocument(binding.PDFContainer2.getRoot());
    }

    private void createPage4() {
        isReadyToGoNextPage = false;
        binding.PDFContainer2.CarInteriorImage.setImageBitmap(FormDataManager.getInstance().getCarInteriorBitmap());
        binding.PDFContainer2.CarLeft.setVisibility(View.GONE);
        binding.PDFContainer2.CarRight.setVisibility(View.GONE);
        binding.PDFContainer2.CarInteriorImage.setVisibility(View.VISIBLE);
        binding.PDFContainer2.CarInteriorImage.post(new Runnable() {
            @Override
            public void run() {
                pdfManager.voidCreatePDFDocument(binding.PDFContainer2.getRoot());
                isReadyToGoNextPage = true;
            }
        });
    }

    private void createPage5() {
        if (FormDataManager.getInstance().getAllImagesBitmap() == null){
            return;
        }
        isReadyToGoNextPage = false;
        binding.FinalPageContentImage.setImageBitmap(FormDataManager.getInstance().getAllImagesBitmap());
        binding.FinalPageContentImage.post(new Runnable() {
            @Override
            public void run() {
                pdfManager.voidCreatePDFDocument(binding.FinalPageContentImage);
                isReadyToGoNextPage = true;
            }
        });
    }

    private void createPage6() {
        binding.FinalPageContentImage.setImageBitmap(null);
        isReadyToGoNextPage = false;
        binding.FinalPageContentImage.setImageBitmap(FormDataManager.getInstance().getFinalImageBitmap());
        binding.FinalPageContentImage.post(new Runnable() {
            @Override
            public void run() {
                pdfManager.voidCreatePDFDocument(binding.FinalPageContentImage);
                isReadyToGoNextPage = true;
            }
        });
    }

    private void createPage7() {
        binding.FinalPageContentImage.setImageBitmap(null);
        isReadyToGoNextPage = false;
        binding.FinalPageContentImage.setImageBitmap(FormDataManager.getInstance().getGeneralNotesImageBitmap());
        binding.FinalPageContentImage.post(new Runnable() {
            @Override
            public void run() {
                pdfManager.voidCreatePDFDocument(binding.FinalPageContentImage);
                isReadyToGoNextPage = true;
            }
        });
    }

    private void init() {

        pdfManager = new PDFManager();
        pdfManager.initPDF(this);


        createPage1();
        showProgress(0);
        generateAllPDFPages();
    }

    private void showProgress(int pageIndex) {

        binding.PreparePDFText.setText(getString(R.string.preparingPage, pageIndex + 1));

        if (pageIndex == 1) {
            binding.PDFContainer1.getRoot().setVisibility(View.INVISIBLE);
        } else if (pageIndex == 4) {
            binding.PDFContainer2.getRoot().setVisibility(View.INVISIBLE);
        } else if (pageIndex == 5) {
            binding.StatusProgress.setVisibility(View.INVISIBLE);
        }
    }


    private void fillPage1Info() {
        FormInformation formInformation = FormDataManager.getInstance().getFormInformation();
        binding.PDFContainer1.OrderIdEditText.setText(formInformation.getOrderID());
        binding.PDFContainer1.NameEditText.setText(formInformation.getUserName());
        binding.PDFContainer1.InspectionDateEditText.setText(DateTimeUtils.getFormattedDate1(formInformation.getInspectionDate()));
        binding.PDFContainer1.EmailEditText.setText(formInformation.getEmail());
        binding.PDFContainer1.PhoneEditText.setText(formInformation.getUserPhoneNumber());
        binding.PDFContainer1.CarPlateEditText.setText(formInformation.getCarPlate());
        binding.PDFContainer1.StreetEditText.setText(formInformation.getStreet());
        binding.PDFContainer1.BuildingNumberEditText.setText(formInformation.getBuildingNumber());

    }
}